from django.apps import AppConfig


class FtDjangoMultichainConfig(AppConfig):
    name = 'ft_django_multichain'
